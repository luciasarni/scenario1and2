class DiceSet

  attr_reader :values

  def initialize
    @values = []
  end

  def roll(number_of_dices)
    @values = Array.new(number_of_dices) { Random.rand(1..6)}
  end

  def values
    @values
  end

end