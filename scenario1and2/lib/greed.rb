# Greed is a dice game where you roll up to five dice to accumulate
# points.  The following "score" function will be used to calculate the
# score of a single roll of the dice.
#
# A greed roll is scored as follows:
#
# * A set of three ones is 1000 points
#
# * A set of three numbers (other than ones) is worth 100 times the
#   number. (e.g. three fives is 500 points).
#
# * A one (that is not part of a set of three) is worth 100 points.
#
# * A five (that is not part of a set of three) is worth 50 points.
#
# * Everything else is worth 0 points.

class Greed
  def score(dice)
    sum = 0
    counts = Hash.new(0)
    dice.each do |value|
      counts[value]+=1
    end

    counts.each do |number,quantity|
      if number==1 && quantity>=3
        sum+=1000
      end
      if number!=1 && quantity>=3
        sum+=100*number
      end
      if number==1 && quantity<3
        sum+=100*quantity
      end
      if number==5 && quantity<3
        sum+=50*quantity
      end
    end

    sum
  end
end
