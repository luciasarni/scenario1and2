require '../lib/dice_set'
require 'test/unit'

class TestDiceSet < Test::Unit::TestCase

  def test_can_create_a_dice_set
    dice = DiceSet.new

    assert_not_nil dice
  end

  def test_rolling_the_dice_returns_an_array
    dice = DiceSet.new

    dice.roll(5)

    assert dice.values.is_a?(Array)
  end

  def test_rolling_the_dice_returns_a_set_of_integers_between_1_and_6
    dice = DiceSet.new

    dice.roll(5)

    dice.values.each do |value|
      assert value >= 1 && value <= 6
    end
  end

  def test_dice_values_do_not_change_unless_explicitly_rolled
    dice = DiceSet.new

    dice.roll(5)
    first_time = dice.values
    second_time = dice.values

    assert_equal first_time.object_id, second_time.object_id
  end

  def test_dice_values_should_change_between_rolls
    dice = DiceSet.new

    dice.roll(5)
    first_time = dice.values
    dice.roll(5)
    second_time = dice.values

    assert_not_equal first_time.object_id, second_time.object_id
  end

  def test_you_can_roll_1_dice
    dice = DiceSet.new

    dice.roll(1)

    assert_equal 1, dice.values.size
  end

  def test_you_can_roll_2_dices
    dice = DiceSet.new

    dice.roll(2)

    assert_equal 2, dice.values.size
  end

  def test_you_can_roll_3_dices
    dice = DiceSet.new

    dice.roll(3)

    assert_equal 3, dice.values.size
  end

  def test_you_can_roll_4_dices
    dice = DiceSet.new

    dice.roll(4)

    assert_equal 4, dice.values.size
  end

  def test_you_can_roll_5_dices
    dice = DiceSet.new

    dice.roll(5)

    assert_equal 5, dice.values.size
  end
end