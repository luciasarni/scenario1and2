require '../lib/greed'
require 'test/unit'

class Test_Greed < Test::Unit::TestCase

  def test_score_of_an_empty_list_is_zero
    greed = Greed.new
    assert_equal 0, greed.score([])
  end

  def test_score_of_a_single_roll_of_5_is_50
    greed = Greed.new
    assert_equal 50, greed.score([5])
  end

  def test_score_of_a_single_roll_of_1_is_100
    greed = Greed.new
    assert_equal 100, greed.score([1])
  end

  def test_score_of_multiple_1s_and_5s_is_the_sum_of_individual_scores
    greed = Greed.new
    assert_equal 300, greed.score([1,5,5,1])
  end

  def test_score_of_single_2s_3s_4s_and_6s_are_zero
    greed = Greed.new
    assert_equal 0, greed.score([2,3,4,6])
  end

  def test_score_of_a_triple_1_is_1000
    greed = Greed.new
    assert_equal 1000, greed.score([1,1,1])
  end

  def test_score_of_other_triples_is_100x
    greed = Greed.new
    assert_equal 200, greed.score([2,2,2])
  end

  def test_score_of_mixed_is_sum
    greed = Greed.new
    assert_equal 250, greed.score([2,5,2,2,3])
  end
end